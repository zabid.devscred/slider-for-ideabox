<?php
$img_size    	= esc_attr( get_post_meta( $id, '_image_size', true ) );
$theme    		= esc_attr( get_post_meta( $id, '_theme', true ) );
$image_ids   	= explode(',', get_post_meta( $id, '_images_ids', true) );
?>

<?php if( count($image_ids) > 0 ): ?>

<div id="ID-<?php echo $id; ?>" class="idebox-test-slider">
	<?php
	foreach ( $image_ids as $image )
	{
		if( ! $image ) continue;

		$image_info = get_post( $image );
		$src 		= wp_get_attachment_image_src( $image, $img_size );
		$thumb 		= wp_get_attachment_image_src( $image, array(50, 50) );
		$caption 	= $image_info->post_excerpt ? $image_info->post_excerpt : '';
		$description = $image_info->post_content ? $image_info->post_content : '';
		
		if (!filter_var($description, FILTER_VALIDATE_URL) === false) {

			echo sprintf('<a href="%1$s"><img src="%2$s" width="%3$s" height="%4$s" data-thumb="%5$s" title="%6$s"></a>',
				$description, $src[0], $src[1], $src[2], $thumb[0], $caption
			);

		} else {

			echo sprintf('<img src="%1$s" width="%2$s" height="%3$s" data-thumb="%4$s" title="%5$s">',
				$src[0], $src[1], $src[2], $thumb[0], $caption
			);
		}
	}
	?>
</div>

<?php endif;

add_action('wp_footer', function() use ( $id ){

	$enable_autoplay	= ( get_post_meta( $id, '_enable_autoplay', true ) == 'on') ? 'true' : 'false';
	?>
	<script type="text/javascript">
		(function($) { 
		  	'use strict';
			$(".idebox-test-slider").each(function() {
			    $(this).owlCarousel({
			        autoPlay: <?php echo $enable_autoplay ?>,
			        items : 1,
			        dots: false,
			        autoplayTimeout: 5000,
			        autoplaySpeed: 5000,
			    });
			});
		})(jQuery);
	</script>
	<?php
}, 60);

?>