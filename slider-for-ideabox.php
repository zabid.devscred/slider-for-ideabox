<?php
/**
 * Plugin Name:       Slider For IdeaBox
 * Plugin URI:        #
 * Description:       This plugin is built for IdeaBox for a test purpose.
 * Version:           1.0
 * Author:            Zabid Ahmed
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       slider-for-ideabox
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) exit;


if ( ! class_exists( 'Slider_For_IdeaBox' ) ) :

    class Slider_For_IdeaBox {

        private $plugin_name    = 'slider-for-ideabox';
        private $plugin_version = '1.0';
        private $plugin_path;
        private $plugin_url;
        protected static $instance = null;

    	public function __construct() {
    		$this->includes();
    		$this->frontend_includes();

            add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
            add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ) );
    	}

        public static function instance() {
            if ( null == self::$instance ) :
                self::$instance = new self;
            endif;
            return self::$instance;
        }

    	public function includes() {
            include_once $this->plugin_path() . '/includes/meta-box.php';
    		include_once $this->plugin_path() . '/includes/admin-panel.php';
            new SFI_Admin_Panel();
    	}

        public function admin_scripts( $hook ) {
            global $post;
            wp_enqueue_media();

            wp_enqueue_script( $this->plugin_name, $this->plugin_url() . '/assets/js/admin.js', array( 'jquery' ), $this->plugin_version, true );
            wp_localize_script( $this->plugin_name, 'IdeaBoxSliderItem', array(
                'ajaxurl'           => admin_url( 'admin-ajax.php' ),
                'post_id'           => isset($post->ID) ? $post->ID : -1,
                'image_ids'         => isset($post->ID) ? get_post_meta( $post->ID, '_images_ids', true ) : null,
                'nonce'             => wp_create_nonce( 'ideabox_image_slider_ajax_nonce' ),
                'create_btn_text'   => __( 'Create Gallery', 'slider-for-ideabox' ),
                'edit_btn_text'     => __( 'Edit Gallery', 'slider-for-ideabox' ),
                'progress_btn_text' => __( 'Saving...', 'slider-for-ideabox' ),
                'save_btn_text'     => __( 'Save Gallery', 'slider-for-ideabox' ),
            ) );
        }

    	/**
    	 * Include frontend files.
    	 */
    	public function frontend_includes(){
    		if( ! is_admin() ){
                include_once $this->plugin_path() . '/includes/shortcode.php';
                new SFI_Shortcodes( $this->plugin_path() );
    		}
    	}

    	public function load_scripts() {
            wp_enqueue_style( 'owl-carousel', $this->plugin_url() . '/assets/css/owl.carousel.css', array(), '1.3.3', 'all' );
            wp_enqueue_script( 'owl-carousel', $this->plugin_url() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '1.3.3', true );
    	}

        /**
         * Plugin path.
         *
         * @return string Plugin path
         */
        private function plugin_path() {
            if ( $this->plugin_path ) return $this->plugin_path;

            return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
        }

        /**
         * Plugin url.
         *
         * @return string Plugin url
         */
        private function plugin_url() {
            if ( $this->plugin_url ) return $this->plugin_url;

            return $this->plugin_url = untrailingslashit( plugin_dir_url( __FILE__ ) );
        }
    }

endif;

Slider_For_IdeaBox::instance();
