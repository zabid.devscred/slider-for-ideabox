<?php

if( ! class_exists( 'SFI_Admin_Panel' ) ):

	class SFI_Admin_Panel {
		public function __construct()
		{
			if (is_admin()) {
				add_action( 'init', array ($this, 'post_type') );
				add_action( 'add_meta_boxes', array( $this, 'meta_box' ) );
		        add_filter( 'post_row_actions', array( $this, 'post_row_actions'), 10, 2 );
				add_filter( 'manage_edit-ideabox_owl_slider_columns', array( $this, 'columns_head') );
				add_filter( 'manage_ideabox_owl_slider_posts_custom_column', array( $this, 'columns_content'), 10, 2 );
			}
		}

		/**
		 * Register a slide post type.
		 * @package slider-for-ideabox
		 * @link http://codex.wordpress.org/Function_Reference/register_post_type
		 */
		public static function post_type() {

			$labels = array(
				'name'                => _x( 'Sliders', 'Post Type General Name', 'slider-for-ideabox'  ),
				'singular_name'       => _x( 'Slider', 'Post Type Singular Name', 'slider-for-ideabox'  ),
				'menu_name'           => __(  'Owl Slider', 'slider-for-ideabox'  ),
				'name_admin_bar'      => __(  'Owl Slider', 'slider-for-ideabox'  ),
				'parent_item_colon'   => __(  'Parent Slider:', 'slider-for-ideabox'  ),
				'all_items'           => __(  'All Sliders', 'slider-for-ideabox'  ),
				'add_new_item'        => __(  'Add New Slider', 'slider-for-ideabox'  ),
				'add_new'             => __(  'Add New', 'slider-for-ideabox'  ),
				'new_item'            => __(  'New Slider', 'slider-for-ideabox'  ),
				'edit_item'           => __(  'Edit Slider', 'slider-for-ideabox'  ),
				'update_item'         => __(  'Update Slider', 'slider-for-ideabox'  ),
				'view_item'           => __(  'View Slider', 'slider-for-ideabox'  ),
				'search_items'        => __(  'Search Slider', 'slider-for-ideabox'  ),
				'not_found'           => __(  'Not found', 'slider-for-ideabox'  ),
				'not_found_in_trash'  => __(  'Not found in Trash', 'slider-for-ideabox'  ),
			);
			$args = array(
				'label'               => __(  'Slider', 'slider-for-ideabox'  ),
				'description'         => __(  'Create slide for your site', 'slider-for-ideabox'  ),
				'labels'              => $labels,
				'supports'            => array( 'title' ),
				'hierarchical'        => false,
				'public'              => false,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 35,
				'menu_icon'           => 'dashicons-images-alt2',
				'show_in_admin_bar'   => false,
				'show_in_nav_menus'   => false,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => true,
				'publicly_queryable'  => true,
				'rewrite'             => false,
				'capability_type'     => 'post',
			);
			register_post_type( 'ideabox_owl_slider', $args );
		}

		public function meta_box()
		{
			$meta_box = array(
			    'id' => 'metabox_for_ideabox_image_slider',
			    'title' => __( 'Owl Carousel Slider', 'slider-for-ideabox' ),
			    'description' => sprintf(
			    	__( 'To use this slider in your posts or pages use the following shortcode: %s', 'slider-for-ideabox' ),
			    	'<input type="text" onmousedown="this.clicked = 1;" onfocus="if (!this.clicked) this.select(); else this.clicked = 2;" onclick="if (this.clicked == 2) this.select(); this.clicked = 0;" value=\'[ideabox_image_slider_shortcode id="'.get_the_ID().'"]\' style="background-color: #f1f1f1; width: 100%; padding: 8px;" />'
			    ),
			    'screen' => 'ideabox_owl_slider',
			    'context' => 'normal',
			    'priority' => 'high',
			    'fields' => array(
			        array(
			            'name' 	=> __( 'Images from Media', 'slider-for-ideabox' ),
			            'desc' 	=> __( 'Choose slider images.', 'slider-for-ideabox' ),
			            'id' 	=> '_images_ids',
			            'type' 	=> 'images',
			            'std' 	=> __( 'Upload Images', 'slider-for-ideabox' )
			        ),
			        array(
			            'name' => __(  'Autoplay', 'slider-for-ideabox' ),
			            'desc' => __( 'Enable Autoplay.', 'slider-for-ideabox' ),
			            'id' => '_enable_autoplay',
			            'type' => 'checkbox',
			            'std' => true
			        )
			    )
			);
			$ideaBoxSlideMeta = new Ideabox_Slider_Meta_Box();
			$ideaBoxSlideMeta->add($meta_box);
		}

		public function post_row_actions( $actions, $post )
		{
			global $current_screen;
		    if( $current_screen->post_type != 'ideabox_owl_slider' ){
				return $actions;
		    }

		    unset( $actions['view'] );
		    unset( $actions['inline hide-if-no-js'] );
			return $actions;
		}

		public function columns_head(){
		    
		    $columns = array(
		        'cb' 			=> '<input type="checkbox">',
		        'title' 		=> __( 'Slide Title', 'slider-for-ideabox' ),
		        'usage' 		=> __( 'Shortcode', 'slider-for-ideabox' ),
		    );

		    return $columns;

		}

		public function columns_content($column, $post_id) {
		    switch ($column) {

		        case 'usage':
		            ?>
						<input
							type="text"
							onmousedown="this.clicked = 1;"
							onfocus="if (!this.clicked) this.select(); else this.clicked = 2;"
							onclick="if (this.clicked == 2) this.select(); this.clicked = 0;"
							value="[ideabox_image_slider_shortcode id='<?php echo $post_id; ?>']"
							style="background-color: #f1f1f1;font-family: monospace;min-width: 250px;padding: 5px 8px;"
						>
		            <?php
		            break;
		        default :
		            break;
		    }
		}
	}

endif;