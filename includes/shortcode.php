<?php
if( ! class_exists('SFI_Shortcodes')):

	class SFI_Shortcodes
	{
		private $plugin_path;

		public function __construct( $plugin_path )
		{
			$this->plugin_path = $plugin_path;

			add_shortcode('ideabox_image_slider_shortcode', array( $this, 'ideabox_image_slider_file' ) );
		}

		/**
		 * A shortcode for rendering the ideabox image slider.
		 *
		 * @param  array   $atts  		Shortcode attributes.
		 * @param  string  $content 	The text content for shortcode. Not used.
		 *
		 * @return string  The shortcode output
		 */
		public function ideabox_image_slider_file( $atts, $content = null )
		{
			extract( shortcode_atts( array( 'id' =>'' ), $atts ) );
			if ( ! $id ) return;

			ob_start();

		    require $this->plugin_path . '/templates/gallery.php';

		    $html = ob_get_contents();
		    ob_end_clean();
		    
		    return apply_filters( 'ideabox_image_slider_shortcode', $html, $id );
		}
	}
endif;