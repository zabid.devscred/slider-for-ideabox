<?php

if ( ! class_exists( 'Ideabox_Slider_Meta_Box' ) ):

	class Ideabox_Slider_Meta_Box {
		public function __construct()
		{
			add_action( 'save_post', array( $this, 'save_meta_boxes' ) );
			add_action( 'wp_ajax_ideabox_slider_saved_images', array( $this, 'save_images' ) );
		}

		/**
		 * Save custom meta box
		 *
		 * @param int $post_id The post ID
		 */
		public function save_meta_boxes( $post_id )
		{
			if (
				! isset($_POST['shapla_meta']) ||
				! isset($_POST['_shapla_nonce']) ||
				! wp_verify_nonce( $_POST['_shapla_nonce'], 'shaplatools_save_meta_box' ) 
			) {
				return;
			}

			// Check if user has permissions to save data.
			if ( !current_user_can( 'edit_post', $post_id ) ){
				return;
			}
			// Check if not an autosave.
	        if ( wp_is_post_autosave( $post_id ) ) {
	            return;
	        }
	        // Check if not a revision.
	        if ( wp_is_post_revision( $post_id ) ) {
	            return;
	        }

			foreach( $_POST['shapla_meta'] as $key => $val ){
				update_post_meta( $post_id, $key, stripslashes(htmlspecialchars($val)) );
			}
		}

		public function save_images()
		{
		    if ( ! isset($_POST['nonce']) || ! wp_verify_nonce( $_POST['nonce'], 'ideabox_image_slider_ajax_nonce' ) ) {
		        return;
		    }

			if ( ! isset( $_POST['post_id'], $_POST['ids'] ) ) {
				return;
			}

			$post_id = $_POST['post_id'];
			// Check if user has permissions to save data.
			if ( !current_user_can( 'edit_post', $post_id ) ){
				return;
			}
			// Check if not an autosave.
	        if ( wp_is_post_autosave( $post_id ) ) {
	            return;
	        }

		    $ids = strip_tags(rtrim($_POST['ids'], ','));
		    update_post_meta( $post_id, '_images_ids', $ids);

		    $thumbs_output = '';
		    foreach( explode(',', $ids) as $thumb ) {
		        $thumbs_output .= sprintf(
		        	'<li style="display: inline-block; margin: 0 5px 5px 0"">%s</li>',
		        	wp_get_attachment_image( $thumb, array( 75, 75 ) )
		        );
		    }
		    echo $thumbs_output;
		    wp_die();
		}

		/**
		 * Add a custom meta box
		 * 
		 * @param array $meta_box Meta box input data
		 */
		public function add( $meta_box )
		{
			if( !is_array( $meta_box) ) return false;

			add_meta_box(
				$meta_box['id'],
				$meta_box['title'],
				array( $this, 'meta_box_callback' ),
				$meta_box['screen'],
				$meta_box['context'] ?: 'advanced',
				$meta_box['priority'] ?: 'high',
				$meta_box
			);
		}

		/**
		 * Create content for the custom meta box
		 * 
		 * @param  WP_Post 	$post
		 * @param  array 	$meta_box
		 * 
		 * @return output metabox content
		 */
		public function meta_box_callback( $post, $meta_box )
		{
			if( ! is_array( $meta_box['args'] ) ) return false;

			$meta_box = $meta_box['args'];

			if( isset($meta_box['description']) && $meta_box['description'] != '' ){
				echo sprintf('<p class="description">%s</p>', $meta_box['description']);
			}

			wp_nonce_field( 'shaplatools_save_meta_box', '_shapla_nonce' );

			$table  = "";
			$table .= "<table class='form-table shapla-metabox-table'>";

			foreach ( $meta_box['fields'] as $field )
			{
				$std_value = isset($field['std']) ? $field['std'] : '';
				$meta = get_post_meta( $post->ID, $field['id'], true );
				$value = $meta ? $meta : $std_value;
				$name = sprintf('shapla_meta[%s]', $field['id']);
				$type = isset($field['type']) ? $field['type'] : 'text';

				$table .= "<tr>";
				$table .= sprintf('<th scope="row"><label for="%1$s">%2$s</label></th>',$field['id'],$field['name']);
				$table .= "<td>";
				
				if (method_exists($this, $type )) {
					$table .= $this->$type($field, $name, $value);
				} else {
					$table .= $this->text($field, $name, $value);
				}

				if (!empty($field['desc'])) {
					$table .= sprintf('<p class="description">%s</p>', $field['desc']);
				}
				$table .= "</td>";
				$table .= "</tr>";
			}

			$table .= "</table>";
			echo $table;
			// $this->color_datepicker_script();
		}

		/**
		 * text input field
		 * 
		 * @param  array $field
		 * @param  string $name
		 * @param  string $value
		 * @return string
		 */
		private function text($field, $name, $value) {
			return sprintf('<input type="text" class="regular-text" value="%1$s" id="%2$s" name="%3$s">', $value, $field['id'], $name);
		}

		/**
		 * checkbox input field
		 * 
		 * @param  array $field
		 * @param  string $name
		 * @param  string $value
		 * @return string
		 */
		private function checkbox($field, $name, $value)
		{
			$checked = ( 'on' == $value ) ? 'checked="checked"' : '';
			$table  = sprintf( '<input type="hidden" name="%1$s" value="off">', $name );
	        $table .= sprintf('<fieldset><legend class="screen-reader-text"><span>%1$s</span></legend><label for="%2$s"><input type="checkbox" value="on" id="%2$s" name="%4$s" %3$s>%1$s</label></fieldset>', $field['name'], $field['id'], $checked, $name);
	        return $table;
		}


		/**
		 * images input field
		 * 
		 * @param  array $field
		 * @param  string $name
		 * @param  string $value
		 * @return string
		 */
		public function images( $field, $name, $value )
		{
			$btn_text 	= $value ? 'Edit Gallery' : 'Add Gallery';
	        $value 		= strip_tags(rtrim($value, ','));
			$output 	= '';

		    if( $value ) {
		        $thumbs = explode(',', $value);
		        foreach( $thumbs as $thumb ) {
		            $output .= '<li style="display: inline-block; margin: 0 5px 5px 0">' . wp_get_attachment_image( $thumb, array(75,75) ) . '</li>';
		        }
		    }

			$html  = '';
			$html .= '<div class="idebox_slider_images">';
			$html .= sprintf('<input type="hidden" value="%1$s" id="ideabox_slider_image_ids" name="_slider_images">', $value);
			$html .= sprintf('<span id="ideabox-slider-upload-btn" class="button button-default">%s</span>', $btn_text);
			$html .= sprintf('<br class="clear"><ul class="ideabox_slider_images_list">%s</ul>', $output);
			$html .= '</div>';

			return $html;
		}
	}

endif;

if( is_admin() ) {
	new Ideabox_Slider_Meta_Box();
}
